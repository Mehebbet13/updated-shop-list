import { SET_APP_DATA } from "../utils/dataStorage";


const SET_LISTS_DATA = "SET_LISTS_DATA";
const ADD_LIST = 'ADD_LIST';
const ADD_LIST_ITEM = 'ADD_LIST_ITEM';
const DELETE_LIST_ITEM = 'DELETE_LIST_ITEM';
const DELETE_LIST = 'DELETE_LIST';
const UPDATE_LIST_ITEM = 'UPDATE_LIST_ITEM';
const ADD_USER_CREDENTIALS = 'ADD_USER_CREDENTIALS';
const TOGGLE_BOUGHT = 'TOGGLE_BOUGHT';
const RESET_BOUGHT_ITEM = 'RESET_BOUGHT_ITEM';


const MODULE_NAME = 'projects';
export const getLists = state => state[MODULE_NAME].lists;
export const getCredentials = state => state[MODULE_NAME].credentials;
const initialState = {
    lists: [
        {
            id: 1,
            title: 'Regular Test',
            isRegular: true,
            listProducts: [{
                id: 1,
                name: 'Pasta',
                amount: `${2}`,
                unit: 'bott',
                isBought: true
            },
                {
                    id: 2,
                    name: 'Salt',
                    amount: `${1}`,
                    unit: 'kq',
                    isBought: true

                },
            ]
        },
        {
            id: 2,
            title: 'OneTime Test',
            isRegular: false,
            listProducts: [

                {
                    id: 1,
                    name: 'Pasta',
                    amount: `${2}`,
                    unit: 'pkg',
                    isBought: true

                },
                {
                    id: 2,
                    name: 'Sugar',
                    amount: `${5}`,
                    unit: 'kg',
                    isBought: true

                },
            ]
        }
    ],
    credentials: {
        username: 'Mika',
        image: ''
    }
};

export function projectsReducer(state = initialState, {type, payload}) {
    switch (type) {
        case SET_APP_DATA:
            return {
                ...state,
                ...payload.projects,
            };
        case SET_LISTS_DATA:
            return {
                ...state,
                ...payload,
            };

        case ADD_LIST:
            return {
                ...state,
                lists: [...state.lists, {
                    id: payload.listId,
                    title: payload.listName,
                    isRegular: payload.isRegular,
                    listProducts: [],

                }]
            }
        case ADD_LIST_ITEM:
            const updatedState = {...state};
            updatedState.lists = [...updatedState.lists];

            const indexOfList = updatedState.lists.findIndex(
                (item) => {
                    return item.id === payload.listId;
                }
            );
            console.log(indexOfList);
            const indexIsFound = indexOfList > -1;
            if (indexIsFound) {
                updatedState.lists[indexOfList] = {
                    ...updatedState.lists[indexOfList],
                    listProducts: [
                        ...updatedState.lists[indexOfList].listProducts,
                        {
                            id: `${Math.random()}${Date.now()}`,
                            name: payload.name,
                            amount: payload.amount,
                            unit: payload.unit,
                            isBought: payload.isBought
                        },
                    ],
                };
            }
            console.log(updatedState)
            return indexIsFound ? updatedState : state;

        case DELETE_LIST_ITEM:
            return {
                ...state,
                lists: state.lists.map((list) => {
                    if (list.id === payload.listId) {
                        return {
                            ...list,
                            listProducts: list.listProducts.filter(
                                (product) => product.id !== payload.id
                            ),
                        }
                    }
                    return list

                })

            }
        case DELETE_LIST:
            return {
                ...state,
                lists: state.lists.filter(

                    (list) => {
                        return list.id !== payload.listId
                    }),

            }
        case UPDATE_LIST_ITEM:
            return {
                ...state,
                lists: state.lists.map((list) => {
                    if (list.id === payload.listId) {
                        return {
                            ...list,
                            listProducts: list.listProducts.map((product) => {
                                    if (product.id === payload.id) {
                                        return {
                                            ...product,
                                            name: payload.name,
                                            amount: payload.amount,
                                            unit: payload.unit
                                        }
                                    }
                                    return product;
                                }
                            ),
                        }
                    }
                    return list

                }),

            };
        case ADD_USER_CREDENTIALS:
            return {
                ...state,
                credentials: {
                    username: payload.username,
                    image: payload.image,
                }
            }
        case TOGGLE_BOUGHT:
            return {
                ...state,
                lists: state.lists.map((list) => {
                    if (list.id === payload.listId) {
                        return {
                            ...list,
                            listProducts: list.listProducts.map((product) => {
                                    if (product.id === payload.id) {
                                        return {
                                            ...product,
                                            isBought: !payload.isBought,
                                        }
                                    }
                                    return product;
                                }
                            ),
                        }
                    }
                    return list

                }),

            };
        case RESET_BOUGHT_ITEM:
            return{
                ...state,
                lists: state.lists.map((list) => {
                    if (list.id === payload.listId) {
                        return {
                            ...list,
                            listProducts: list.listProducts.map((product) => {
                                    return {
                                        ...product,
                                        isBought: false,
                                    }
                                }
                            ),
                        }
                    }
                    return list

                }),

            }
        default:
            return state;
    }
}

export const addList = (payload) => ({
    type: ADD_LIST,
    payload
});

export const addListItem = (payload) => ({
    type: ADD_LIST_ITEM,
    payload
});
export const deleteListItem = (payload) => ({
    type: DELETE_LIST_ITEM,
    payload
});
export const deleteList = (payload) => ({
    type: DELETE_LIST,
    payload
});
export const updateListItem = (payload) => ({
    type: UPDATE_LIST_ITEM,
    payload
});
export const addUserCredentials = (payload) => ({
    type: ADD_USER_CREDENTIALS,
    payload
});
export const toggleBought = (payload) => ({
    type: TOGGLE_BOUGHT,
    payload
});
export const resetBoughtItem = (payload) => ({
    type: RESET_BOUGHT_ITEM,
    payload
});
export const setListData = (payload) => ({
    type: SET_LISTS_DATA,
    payload,
});


