import {createStore,  combineReducers, applyMiddleware } from 'redux';
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import {projectsReducer} from "./project";
import {getDataFromAS, updateAS} from "../utils/dataStorage";


const rootReducers = combineReducers({
    projects:projectsReducer
});


const store = createStore(
    rootReducers,
    composeWithDevTools(applyMiddleware(thunk)),
);

store.subscribe(() => {
    updateAS(store);
    console.log(store.getState());
});

getDataFromAS(store);

export default store;
