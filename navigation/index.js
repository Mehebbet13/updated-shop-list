import React from 'react';
import {NavigationContainer} from '@react-navigation/native'
import {createDrawerNavigator} from '@react-navigation/drawer'
import {DrawerContent} from "../components/DrawerContent";
import {OneTimeStack} from "./OneTimeStack";
import {CreateStack} from "./CreateStack";
import {UserSettingsStack} from "./UserSettingsStack";
import {RegularStack} from "./RegularStack";
import {ListStack} from "./ListStack";

const {Navigator, Screen}=createDrawerNavigator();
export const Drawer = () => {
    return (
        <NavigationContainer>
            <Navigator  drawerContent={props => <DrawerContent {...props}/> }>
                <Screen name="OneTimeStack" component={OneTimeStack} />
                <Screen name="RegularStack" component={RegularStack} />
                <Screen name="CreateStack" component={CreateStack} />
                <Screen name="UserSettingsStack" component={UserSettingsStack} />
                <Screen name="ListStack" component={ListStack} />

            </Navigator>
        </NavigationContainer>
    );
};

