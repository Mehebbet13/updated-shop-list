import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { UserSettings} from "../screens";
const { Navigator, Screen } = createStackNavigator();

export const UserSettingsStack = () => {
    return (
        <Navigator>
            <Screen
                name="USER SETTINGS"
                component={UserSettings}
                options={{headerShown : false }}
            />

        </Navigator>
    );
}



