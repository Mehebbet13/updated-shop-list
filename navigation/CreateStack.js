import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import {CreateList} from "../screens";
const { Navigator, Screen } = createStackNavigator();

export const CreateStack = () => {
    return (
        <Navigator>
            <Screen
                name="CreateList"
                component={CreateList}
                options={{headerShown : false }}
            />

        </Navigator>
    );
}



