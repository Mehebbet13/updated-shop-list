import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import {SingleItemEdit,SingleList,SingleListEdit} from "../screens";
const { Navigator, Screen } = createStackNavigator();

export const ListStack = () => {
    return (
        <Navigator>
            <Screen
                name="SingleItemEdit"
                component={SingleItemEdit}
                options={{headerShown : false }}
            />
            <Screen
                name="SingleListEdit"
                component={SingleListEdit}
                options={{headerShown : false }}
            />
            <Screen
                name="SingleList"
                component={SingleList}
                options={{headerShown : false }}
            />

        </Navigator>
    );
};
