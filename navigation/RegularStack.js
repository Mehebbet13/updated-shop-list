import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import {HomeScreen2} from "../screens";
const { Navigator, Screen } = createStackNavigator();

export const RegularStack = () => {
    return (
        <Navigator>
            <Screen
                name="REGULAR LISTS"
                component={HomeScreen2}
                options={{headerShown : false }}
            />

        </Navigator>
    );
};
