import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import {HomeScreen} from "../screens";
const { Navigator, Screen } = createStackNavigator();

export const OneTimeStack = () => {
    return (
        <Navigator>
            <Screen
                name="ONE TIME LIST"
                component={HomeScreen}
                options={{headerShown : false }}
            />
        </Navigator>
    );
};
