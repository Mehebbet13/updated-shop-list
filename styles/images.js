import back from '../assets/back.png';
import save from '../assets/save.png';
import edit from '../assets/edit.png';
import remove from '../assets/remove.png';
import sidebar from '../assets/sidebar.png';
import userImg from '../assets/placeholderAvatar.png'

export const IMAGES = {
    edit,
    remove,
    save,
    back,
    sidebar,
    userImg,
};
