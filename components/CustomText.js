import React from "react";
import { Text } from "react-native";

const fontFamily = {
    regular: "MontserratRegular",
    medium: "MontserratMedium",
    bold: "MontserratBold",

};

export const CustomText = ({ children, style, weight, ...rest }) => {
    return (
        <Text
            {...rest}
            style={[
                {
                    fontFamily: fontFamily[weight] || fontFamily.regular,
                },
                style,
            ]}
        >
            {children}
        </Text>
    );
};
