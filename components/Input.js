import React from 'react';
import {StyleSheet, TextInput, View} from 'react-native';
import {COLORS} from "../styles/colors";
export const Input = ({placeholder,onChangeText,value}) => {
    return (
        <View style={styles.textInput}>
            <TextInput style={styles.placeholder} placeholder={placeholder} onChangeText={onChangeText} value={value}/>
        </View>
    );
};
const styles = StyleSheet.create({
    textInput: {
        width: '90%',
        backgroundColor: COLORS.gray,
        borderRadius: 45,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 6,
        paddingBottom: 6,
        textAlign: 'center'
    },
    placeholder: {
        textAlign: 'center',

    },
});
