import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {CustomText} from "./CustomText";
export const Button = ({btnName,btnColor,btnTextColor,btnWidth,onPress,fontSize}) => {
    return (
        <TouchableOpacity style={[styles.btn,{backgroundColor:btnColor,width:btnWidth}]} onPress={onPress}>
            <CustomText weight={'bold'} style={[styles.btnName,{color:btnTextColor,fontSize:fontSize}]}>
                {btnName}
            </CustomText>

        </TouchableOpacity>
    );
};
const styles = StyleSheet.create({
    btn: {
        padding: 13,
        borderRadius: 45,
        marginTop: 15,
        marginBottom: 10,
        textAlign: 'center'
    },
    btnName: {
        fontStyle: 'normal',
        lineHeight: 17,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',

    },
});
