import { AsyncStorage } from "react-native";

export const SET_APP_DATA = "SET_APP_DATA";
const setAppData = (payload) => ({
    type: SET_APP_DATA,
    payload,
});

const AS_DATA_KEY = "shop-list";

export async function updateAS(store) {
    const state = store.getState();
    await AsyncStorage.setItem(AS_DATA_KEY, JSON.stringify(state));
    console.log(state)
}

export async function getDataFromAS(store) {
    const dataJSON = await AsyncStorage.getItem(AS_DATA_KEY);
    if (dataJSON) {
        const data = JSON.parse(dataJSON);
        console.log(data)
        store.dispatch(setAppData(data));
    }
}
