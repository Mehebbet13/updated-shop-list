import React, {useState, useEffect} from 'react';
import {StatusBar, StyleSheet, TextInput, TouchableOpacity, View} from 'react-native';
import {ScreensBackground} from "../components/ScreensBackground";
import {CustomText} from "../components/CustomText";
import {connect} from "react-redux";
import {addList, getLists} from "../store/project";
import {COLORS} from "../styles/colors";
import {Button} from "../components/Button";
import {Input} from "../components/Input";

const mapStateToProps = state => ({
    lists: getLists(state)
});
export const CreateList = connect(mapStateToProps, {addList})((props) => {
    const [listNameAndIsRegular, setListNameAndIsRegular] = useState({
        listId:`${Math.random()}${Date.now()}`,
        isRegular: false,
        listName: '',
    });

    const [opacityForOneTime, setOpacityForOneTime] = useState(false);
    const [opacityForRegular, setOpacityForRegular] = useState(false);

    const handleRegular = (name, value) => {
        setOpacityForRegular(!opacityForRegular);
        setOpacityForOneTime(false);
        setListNameAndIsRegular((listNameAndIsRegular) => ({
            ...listNameAndIsRegular,
            [name]: value

        }));

    };
    const handleOneTime = (name, value) => {
        setOpacityForOneTime(!opacityForOneTime);
        setOpacityForRegular(false);
        setListNameAndIsRegular((listNameAndIsRegular) => ({
            ...listNameAndIsRegular,
            [name]: value

        }));

    };
    const createList = () => {
        props.addList(listNameAndIsRegular);
            props.navigation.navigate("ListStack", {screen: 'SingleListEdit',
                params:
               {
                listTitle:listNameAndIsRegular.listName,
                isRegular:listNameAndIsRegular.isRegular,
                listId:listNameAndIsRegular.listId,
            }});
            setListNameAndIsRegular({
                listId:`${Math.random()}${Date.now()}`,
                isRegular: false,
                listName: '',
            });
        setOpacityForOneTime(false)
        setOpacityForRegular(false)

    };
    const handleName = (name, value) => {
        setListNameAndIsRegular((listNameAndIsRegular) => ({
            ...listNameAndIsRegular,
            [name]: value

        }));
    }

    return (
        <ScreensBackground title={'New List'}>
            <StatusBar translucent/>
            <View style={{width: '100%'}}>
                <CustomText weight={'bold'} style={styles.listName}>list name</CustomText>
            </View>
            <Input value={listNameAndIsRegular.listName} onChangeText={(text) => handleName('listName', text)} placeholder={'Something for me'}/>

            <View style={styles.btnContainer}>
                <View style={{opacity: opacityForOneTime ? 1 : 0.5}}>
                    <Button btnColor={COLORS.gray}
                            btnWidth={140}
                            btnName={'One Time'}
                            btnTextColor={COLORS.black}
                            onPress={() => handleOneTime('isRegular', false)}
                    />
                </View>
                <View style={{opacity: opacityForRegular ? 1 : 0.5}}>
                    <Button btnColor={COLORS.gray}
                            btnWidth={140}
                            btnName={'Regular'}
                            btnTextColor={COLORS.black}
                            onPress={() => handleRegular('isRegular', true)}
                    />
                </View>
            </View>
            <Button btnColor={COLORS.main}
                    btnWidth={'90%'}
                    btnName={'CREATE LIST'}
                    btnTextColor={COLORS.secondary}
                    fontSize={14}
                    onPress={() => createList()}
            />
        </ScreensBackground>
    );
})

export const styles = StyleSheet.create({
    listName: {
        marginTop: 10,
        marginBottom: 6,
        fontStyle: 'normal',
        fontSize: 12,
        lineHeight: 15,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        color: COLORS.black,
        opacity: 0.65,
    },
    textInput: {
        width: '90%',
        backgroundColor: COLORS.gray,
        borderRadius: 45,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 6,
        paddingBottom: 6,
        textAlign: 'center'
    },
    placeholder: {
        textAlign: 'center',

    },
    btnContainer: {
        display: 'flex',
        flexDirection: 'row',
    },
    btn: {
        width: 140,
        padding: 15,
        backgroundColor: COLORS.gray,
        borderRadius: 45,
        marginTop: 10,
        textAlign: 'center'
    },
    btnName: {
        fontStyle: 'normal',
        fontSize: 12,
        lineHeight: 15,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        color: '#000000'
    },
    longBtn: {
        width: '90%',
        padding: 13,
        backgroundColor: COLORS.main,
        borderRadius: 45,
        marginTop: 15,
        marginBottom: 10,
        textAlign: 'center'
    },
    longBtnName: {
        fontStyle: 'normal',
        fontSize: 14,
        lineHeight: 17,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        color: COLORS.secondary,
        textTransform: 'uppercase',

    },

});
