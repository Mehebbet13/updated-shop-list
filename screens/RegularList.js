import React,{useState} from 'react';
import {FlatList} from 'react-native';
import {ScreensBackground} from "../components/ScreensBackground";
import {IMAGES} from '../styles/images'
import {SingleItem} from "../components/SingleItem";
import {getLists,deleteList} from "../store/project";
import {connect} from "react-redux";
import ModalComp from "../components/Modal";

const mapStateToProps = state => ({
    lists: getLists(state)
})
export const HomeScreen2 = connect(mapStateToProps,{deleteList})((props) => {
    const {lists} = props;
    const regularLists=lists.filter((regularList)=>regularList.isRegular===true);
    const [isModalOpen, setModalOpen] = useState(false);
    const [id, setId] = useState('');

    const handleDeleteList = (listId) => {
        props.deleteList({
            listId: listId,
        });
        hideModal();
    };
    const hideModal=()=>{
        setModalOpen(!isModalOpen);
    };
    const openModal=(id)=>{
        setModalOpen(!isModalOpen);
        setId(id);

    };
    return (
        <>
            <ScreensBackground title={'Regular Lists'} image2={IMAGES.sidebar} onPress2={() => props.navigation.openDrawer() }>
                <FlatList
                    data={regularLists}
                    renderItem={({item}) =>
                        <SingleItem key={item.id}
                                    listId={item.id}
                                    isRegular={item.isRegular}
                                    onLongPress={()=>openModal(item.id)}
                                    handleDeleteList={(listId)=>handleDeleteList(listId)}
                                    boughtItemsSize={item.listProducts.filter((product)=>product.isBought===true).length}
                                    listTitle={item.title}
                                    listProductsLength={item.listProducts.length}
                                    onPress={(listId, isRegular, listTitle) =>
                                        props.navigation.navigate("ListStack", {screen: 'SingleList',
                                            params:
                                                {
                                                    listTitle: listTitle,
                                                    listId: listId,
                                                    isRegular: isRegular,
                                                    // isBought:item.listProducts.filter((product)=>product.isBought===true)
                                                    indexOfList: lists.findIndex(
                                                        (list) => {
                                                            return list.id === listId
                                                        }
                                                    )
                                                }
                                        })
                                    }
                        />
                    }
                    keyExtractor={item => `${item.id}`}
                />

            </ScreensBackground>
            { isModalOpen?
                <ModalComp hideModal={()=>hideModal()}  handleDeleteList={()=>handleDeleteList(id)}/>
                :null
            }
        </>
    );
})
