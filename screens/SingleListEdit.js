import React, {useState} from 'react';
import {StyleSheet,Alert, TextInput, TouchableOpacity, Image, View, FlatList, Keyboard} from 'react-native';
import {ScreensBackground} from "../components/ScreensBackground";
import {IMAGES} from '../styles/images'
import {CustomText} from "../components/CustomText";
import {connect} from "react-redux";
import {getLists, addListItem, deleteListItem,updateListItem} from "../store/project";
import {COLORS} from "../styles/colors";
import {Button} from "../components/Button";

const mapStateToProps = state => ({
    lists: getLists(state)
});
export const SingleListEdit = connect(mapStateToProps, {addListItem,updateListItem, deleteListItem})((props) => {
    const {listTitle} = props.route.params;
    const {isRegular} = props.route.params;
    const {listId} = props.route.params;
    const {lists} = props;
    const options = ['pkg', 'kg', 'litre', 'bott'];
    const [unitIdx, setUnitIdx] = useState('');
    const [isEditMode, setIsEditMode] = useState(false);
    const [listItems, setListItems] = useState({
        id: '',
        listId: listId,
        name: '',
        amount: 0,
        unit: '',
        isBought: false
    });
    const indexOfList = lists.findIndex(
        (list) => {
            return list.id === listId
        }
    );

    const handleListItems = (name, value) => {
        setListItems(items => ({
            ...items,
            [name]: value
        }));
        console.log(value)
        if (name === 'unit') {
            foundUnit(value);

        }
    };
    const createListItem = () => {
        props.addListItem(listItems);
        Keyboard.dismiss();
        setListItems({
            id: '',
            listId: listId,
            name: '',
            amount: 0,
            unit: '',
        });
        setUnitIdx('');
    };
   const  deleteItem =(id)=>{
       props.deleteListItem({
           listId: listId,
           id,
       });
   };
    const handleDeleteItem = (id) => {

        Alert.alert("Delete product?", "Are you sure?", [
            { text: "No", style: "cancel" },
            { text: "Yes, delete", onPress: deleteItem(id) },
        ]);

    };
    const handleUpdate = () => {
        props.updateListItem({
            id:listItems.id,
            listId: listId,
            name: listItems.name,
            amount: listItems.amount,
            unit: listItems.unit,
        });
        Keyboard.dismiss();
        setIsEditMode(false);
    };
    const handleCancel = () => {
        setIsEditMode(false);
    };
    const handleEdit = (id,name,unit, amount) => {
        setListItems({
            id:id,
            name: name,
            amount: amount,
            unit: unit,
        });
        foundUnit(unit);

        setIsEditMode(true);

    };
    function foundUnit(unit) {
        const idx = options.indexOf(unit);
        setUnitIdx(`${idx}`);
    }

    return (
        <ScreensBackground title={listTitle}
                           image1={IMAGES.back}
                           image2={IMAGES.save}
                           onPress1={() => props.navigation.navigate('ONE TIME LIST')}
                           onPress2={() => props.navigation.navigate('SingleList', {
                               listTitle: listTitle,
                               listId: listId,
                               indexOfList: indexOfList,
                               isRegular: isRegular

                           })}
        >
            <View style={styles.addingProductSection}>
                <View>
                    <CustomText style={styles.positionName}>position name</CustomText>
                    <View style={styles.textInput}>
                        <TextInput value={listItems.name} onChangeText={(text) => handleListItems('name', text)}/>
                    </View>
                </View>
                <View>
                    <CustomText style={styles.count}>count</CustomText>
                    <View style={styles.counterContainer}>
                        <TouchableOpacity style={styles.minus}
                                          onPress={() => handleListItems('amount', listItems.amount < 1 || isNaN(listItems.amount) ? 0 : listItems.amount - 1)}>
                            <CustomText style={{fontSize: 27, fontWeight: 'bold'}}>-</CustomText>
                        </TouchableOpacity>
                        <View>
                            <CustomText style={{fontSize: 16, fontWeight: 'bold'}}>{listItems.amount}</CustomText>
                        </View>
                        <TouchableOpacity onPress={() => handleListItems('amount', listItems.amount + 1)}>
                            <CustomText style={{fontSize: 20, fontWeight: 'bold'}}>+</CustomText>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.options}>
                    {options.map((option, ind) => (
                        <View style={[styles.optionBtn, {opacity: (unitIdx === `${ind}`) ? 1 : 0.5}]}>
                            <TouchableOpacity key={`${option}${Date.now}`}
                                              onPress={() => handleListItems('unit', option)}
                            >
                                <CustomText weight={'bold'}>{option}</CustomText>
                            </TouchableOpacity>
                        </View>
                    ))}
                </View>

                    {isEditMode ?
                        <View style={styles.btnContainer}>
                            <View>
                                <Button btnColor={COLORS.main}
                                        btnWidth={140}
                                        btnName={'CANCEL'}
                                        btnTextColor={COLORS.secondary}
                                        fontSize={14}
                                        onPress={() =>handleCancel()
                                        }
                                />
                            </View>
                            <View style={{marginLeft: 10}}>
                                <Button btnColor={COLORS.main}
                                        btnWidth={140}
                                        btnName={'UPDATE'}
                                        btnTextColor={COLORS.secondary}
                                        fontSize={14}
                                        onPress={() =>handleUpdate()}
                                />
                            </View>
                        </View>
                        :
                <View style={styles.btn}>
                        <Button btnColor={COLORS.main}
                                btnWidth={'90%'}
                                btnName={'ADD TO LIST'}
                                btnTextColor={COLORS.secondary}
                                fontSize={14}
                                onPress={() => createListItem()}
                        />
                </View>
                    }

            </View>
            <FlatList
                data={lists[indexOfList].listProducts}
                renderItem={({item}) =>
                    <View style={[styles.item]}>
                        <TouchableOpacity style={styles.edit}
                                          activeOpacity={0.7}
                                          onPress={() => handleEdit(item.id,item.name,item.unit,item.amount)}>
                            <Image source={IMAGES.edit}/>
                        </TouchableOpacity>
                        <CustomText style={styles.productName}>{item.name}</CustomText>
                        <CustomText style={styles.productAmount}>x{item.amount} {item.unit}</CustomText>
                        <TouchableOpacity style={styles.remove}
                                          activeOpacity={0.7}
                                          onPress={() => handleDeleteItem(item.id)}
                        >
                            <Image source={IMAGES.remove}/>
                        </TouchableOpacity>
                    </View>
                }
                keyExtractor={item => item.id}
            />


        </ScreensBackground>
    );
});

export const styles = StyleSheet.create({
    positionName: {
        position: 'absolute',
        left: 67,
        top: 16,
        fontStyle: 'normal',
        fontSize: 12,
        lineHeight: 15,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        color: COLORS.black,
        opacity: 0.75,
    },
    textInput: {
        position: 'absolute',
        width: 200,
        height: 35,
        left: 12,
        top: 40,
        padding: 10,
        backgroundColor:COLORS.gray,
        borderRadius: 45,
        textAlign: 'center',

    },
    count: {
        position: 'absolute',
        right: 40,
        top: 16,
        fontStyle: 'normal',
        fontSize: 12,
        lineHeight: 15,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        color:COLORS.black,
        opacity: 0.75,
    },
    counterContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        position: 'absolute',
        width: 90,
        height: 38,
        padding: 12,
        right: 10,
        top: 40,
        backgroundColor:COLORS.gray,
        borderRadius: 45,
    },

    btn: {
        width: '100%',
        borderRadius: 45,
        position: 'absolute',
        top: 150,
        left: 16

    },
    longBtnName: {
        fontStyle: 'normal',
        fontSize: 14,
        lineHeight: 17,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        color:COLORS.secondary,
        textTransform: 'uppercase',

    },
    addingProductSection: {
        width: '100%',
        height: 220,
        borderBottomWidth: 2,
        borderBottomColor: '#E5E5E5',
        marginBottom: 30
    },

    minus: {
        marginRight: 4,
        marginBottom: 4

    },
    options: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        position: 'absolute',
        padding: 12,
        top: 70,
    },
    optionBtn: {
        width: 70,
        height: 38,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 12,
        backgroundColor:COLORS.gray,
        borderRadius: 45,
        marginTop: 10,

    },
    item: {
        width: 300,
        backgroundColor:COLORS.secondary,
        borderRadius: 45,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 16,
        paddingBottom: 16,
        marginBottom: 20,
        position: 'relative',
        borderWidth: 2,
        borderColor:COLORS.lightYellow,
        display: 'flex',
        justifyContent: 'center',


    },
    productName: {
        fontStyle: 'normal',
        fontSize: 14,
        lineHeight: 17,
        position: 'absolute',
        top: 8,
        left: 50,
        color: COLORS.black,
    },
    productAmount: {
        fontStyle: 'normal',
        fontSize: 14,
        lineHeight: 17,
        position: 'absolute',
        top: 8,
        right: 50,
        color: COLORS.black,
    },
    edit: {
        width: 20,
        paddingLeft: 17,
        paddingRight: 17,
        paddingTop: 7,
        paddingBottom: 7,
        borderRadius: 20,
        backgroundColor:COLORS.yellow,
        position: 'absolute',
        left: -1,
        bottom: -1,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',

    },
    remove: {
        width: 20,
        paddingLeft: 17,
        paddingRight: 17,
        paddingTop: 7,
        paddingBottom: 7,
        borderRadius: 20,
        backgroundColor:COLORS.main,
        position: 'absolute',
        right: -1,
        bottom: -1,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btnContainer: {
        width:'100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent:'space-around',
        position: 'absolute',
        top: 150,
    },
    btnName: {
        fontStyle: 'normal',
        fontSize: 13,
        lineHeight: 15,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        color:COLORS.secondary
    },


});
